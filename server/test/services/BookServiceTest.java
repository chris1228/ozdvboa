package services;

import renameMeLater.ModelsTest;
import models.Book;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.*;
import static play.test.Helpers.*;

/**
 * UnitTest for BookService. Database is supposed to be empty.
 *
 * @author Olivier Norture <olivier@norture.fr>
 */
public class BookServiceTest {

    private final String title = "Book 1";

    /**
     * Test for insert in DataBase. Test to insert two books, the first should
     * work. The second should throw an exception.
     */
    @Test(expected = Exception.class)
    public void testInsert() {
        running(fakeApplication(ModelsTest.dbData()), () -> {
            try {
                //Add the first book
                Book b = new Book(title);
                BookService.insert(b);

                //Try to add new book
                Book b2 = new Book("Book 2");
                BookService.insert(b2);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    /**
     * Test for getBook.
     */
    @Test
    public void testGetBook() {
        running(fakeApplication(ModelsTest.dbData()), () -> {
            try {
                Book b = new Book("Book 1");
                BookService.insert(b);
            } catch (Exception e) {
                //OSEF
            }
            Book b = BookService.getBook();
            assertThat(b != null).isTrue();
        });
    }

    /**
     * Test for update Book in the DataBase.
     */
    @Test
    public void testUpdate() {
        running(fakeApplication(ModelsTest.dbData()), () -> {
            try {
                Book b = BookService.getBook();
                String newTitle = "Book 1 update title";
                b.setTitle(newTitle);
                BookService.update(b);

                Book b2 = BookService.getBook();
                assertThat(b2.getTitle().equals(newTitle)).isTrue();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    /**
     * Test for delete Book in the DataBase.
     */
    @Test
    public void testDelete() {
        running(fakeApplication(ModelsTest.dbData()), () -> {
            try {
                try {
                    Book b = new Book("Book 1");
                    BookService.insert(b);
                } catch (Exception e) {
                    //OSEF
                }
                BookService.delete(BookService.getBook());
                assertThat(BookService.getBook() == null).isTrue();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
    }

}
