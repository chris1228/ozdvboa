import com.avaje.ebean.Ebean;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import models.Book;
import static org.fest.assertions.Assertions.assertThat;

import models.Chapter;
import org.junit.*;
import static play.test.Helpers.*;

public class ModelsTest {

    public static Map<String, String> dbData() {
        //Map for postgres database connection
        final HashMap<String, String> postgres = new HashMap<>();
        postgres.put("db.default.driver", "org.postgresql.Driver");
        postgres.put("db.default.url", "jdbc:postgresql://localhost:5432/edtc");
        postgres.put("db.default.user", "edtc");
        postgres.put("db.default.password", "edtc");
        return postgres;
    }

    @Test
    public void testDataBaseConnection() {
        //Create a fake application with the postgres database connection
        running(fakeApplication(dbData()), () -> {
            //DataBase test ...
            List<Book> books = Book.find.all();
            assertThat(books.isEmpty()).isFalse();
        });
    }

    @Test(expected = Exception.class)
    public void testCreateBookWithEmptyTitle() {
        running(fakeApplication(dbData()), () -> {
            try {
                Book b = new Book("");
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
    }


    @Test(expected = Exception.class)
    public void testCreateChapterWithEmptyTitle() {
        running(fakeApplication(dbData()), () -> {
            try {
                Chapter chapter = new Chapter("");
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    @Test
    public void testGeneratedId() {
        running(fakeApplication(dbData()), () -> {
            try {
                Book b = new Book("Book Test");
                Ebean.save(b);
                assertThat(b.getId()).isGreaterThan(0);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    @Test
    public void testRemoveBook() {
        running(fakeApplication(dbData()), () -> {
            try {
                Book b = new Book("Book Test");
                Ebean.save(b);
                assertThat(b.getId()).isGreaterThan(0);
                Ebean.delete(b);
                b = Book.find.byId(b.getId());
                assertThat(b == null).isTrue();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
    }


}
