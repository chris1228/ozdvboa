import models.Book;
import models.Chapter;
import models.Paragraph;
import models.Volume;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;

/**
 * Created by cedric Baulamon on 01/02/15.
 */
public class AttachTest {
    @Test(expected = Exception.class)
    public void chapterAttachNull(){
        running(fakeApplication(ModelsTest.dbData()), () -> {
            Chapter c = null;
            try {
                c = new Chapter("Test");
            } catch (Exception e) {
               throw new RuntimeException(e.getMessage());
            }
            c.attach(null);
        });
    }

    @Test
    public void chapterAttach(){
        running(fakeApplication(ModelsTest.dbData()), () -> {
            Chapter c = null;
            try {
                c = new Chapter("Test");
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
            Paragraph paragraph = new Paragraph();
            c.attach(paragraph);
            assertTrue(c.getParagraphs().contains(paragraph));
        });
    }

    @Test(expected = Exception.class)
      public void volumeAttachNull(){
        running(fakeApplication(ModelsTest.dbData()), () -> {
            Volume volume = new Volume("Test");
            volume.attach(null);
        });
    }

    @Test
    public void volumeAttach(){
        running(fakeApplication(ModelsTest.dbData()), () -> {
            Volume volume = new Volume("Test");
            Chapter chapter = null;
            try {
                chapter = new Chapter("test");
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
            volume.attach(chapter);
            assertTrue(volume.getChapters().contains(chapter));
        });
    }

    @Test(expected = Exception.class)
    public void bookAttachNull(){
        running(fakeApplication(ModelsTest.dbData()), () -> {
            Book book = null;
            try {
                book = new Book("Test");
            } catch (Exception e) {
               throw new RuntimeException(e.getMessage());
            }
            book.attach(null);
        });
    }

    @Test
    public void bookAttach(){
        running(fakeApplication(ModelsTest.dbData()), () -> {
            Book book = null;
            try {
                book = new Book("Test");
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
            Volume volume = new Volume("test");
            book.attach(volume);
            assertTrue(book.getVolumes().contains(volume));
        });
    }





}
