package models;

import org.joda.time.DateTime;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "token")
public class MyToken extends Model {
    @Id
    public String uuid;
    public String email;
    public DateTime creationTime;
    public DateTime expirationTime;
    public boolean isSignUp;

    public MyToken(String uuid, String email, DateTime creationTime, DateTime expirationTime, boolean isSignUp){
        this.uuid = uuid;
        this.email = email;
        this.creationTime = creationTime;
        this.expirationTime = expirationTime;
        this.isSignUp = isSignUp;
    }

    public MyToken(securesocial.core.java.Token token){
        this.uuid = token.getUuid();
        this.email = token.getEmail();
        this.creationTime = token.getCreationTime();
        this.expirationTime = token.getExpirationTime();
        this.isSignUp = token.getIsSignUp();
    }

    public static Finder<Long, MyToken> find = new Finder<>(
            Long.class, MyToken.class
    );

    public securesocial.core.java.Token toSSToken() {
        return securesocial.core.java.Token.fromScala(
                securesocial.core.providers.MailToken$.MODULE$.apply(
                        uuid, email, creationTime, expirationTime, isSignUp
                )
        );
    }

    @Override
    public String toString() {
        return "MyToken{" +
                "uuid='" + uuid + '\'' +
                ", email='" + email + '\'' +
                ", creationTime=" + creationTime +
                ", expirationTime=" + expirationTime +
                ", isSignUp=" + isSignUp +
                '}';
    }
}
