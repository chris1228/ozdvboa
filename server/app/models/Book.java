package models;

import java.io.Serializable;
import play.db.ebean.Model;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * Book model representation.
 * @author Olivier Norture <olivier@norture.fr>
 */
@Entity
@Table(name = "book")
public class Book extends Model implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    private List<Volume> volumes;
    
    @NotNull
    private String title;

    /****************************************************************************************************************/

    /**
     * Get the book's id.
     * @return the book's id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the book's id.
     * @param id the book's id.
     */
    public void setId(Long id) {
        Objects.requireNonNull(id);
        this.id = id;
    }

    /**
     * Get the book's title.
     * @return the book's title.
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * Set the book's title.
     * The title must be not set and not empty.
     * @param title the new book's title.
     * @throws Exception if the title is null or empty.
     */
    public void setTitle(String title){
        Objects.requireNonNull(title);
        if(title.isEmpty()){
            throw new RuntimeException("Title must be set");
        }
        this.title = title;
    }

    /**
     * Get all book's volumes list.
     * @return the book's volumes list.
     */
    public List<Volume> getVolumes() {
        volumes.sort(new Comparator<Volume>() {
            @Override
            public int compare(Volume o1, Volume o2) {
                Long o1id = o1.getId();
                Long o2id = o2.getId();
                if(o1id == null) { o1id = 0L ; }
                if(o2id == null) { o2id = 0L ; }
                return Long.compare(o1id, o2id);
            }
        });
        return volumes;
    }

    /**
     * Set the book's volumes list.
     * @param volumes the book's volumes list to set.
     */
    public void setVolumes(List<Volume> volumes) {
        Objects.requireNonNull(volumes);
        this.volumes = volumes;
    }
    

    /****************************************************************************************************************/

    protected Book() {
        volumes = new ArrayList<>();
    }

    /**
     * Create a new book with the given title.
     * @param title the book's title.
     * @throws Exception if the title is null or empty.
     */
    public Book(String title) {
        this();
        setTitle(title);
    }

    /**
     * Add the given volume to this book.
     * @param volume the volume to add.
     */
    public void attach(Volume volume) {
        Objects.requireNonNull(volume);
        this.volumes.add(volume);
    }
    
    /**
     * The Finder for request in the DataBase.
     */
    public static Finder<Long, Book> find = new Finder<>(
            Long.class, Book.class
    );

}
