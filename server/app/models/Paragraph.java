package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.security.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "paragraphs")
public class Paragraph extends Model {
    @Id
    @Constraints.Min(10)
    @GeneratedValue
    private Long id;

    @OneToOne
    private User lockedBy;

    @ManyToOne
    private Chapter chapter; // Parent : chapter which contains the current paragraph

    @ManyToMany
    private List<Group> groups = new ArrayList<>(); // Groups allowed to access this paragraph

    @ManyToMany
    private Set<User> assignees = new HashSet<>(); // Users allowed to edit this paragraph

    private String title;
    private String content;
    private Timestamp lastLockTime;
    private String state ;

    /****************************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public User getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(User lockedBy) {
        this.lockedBy = lockedBy;
    }

    public Timestamp getLastLockTime() {
        return lastLockTime;
    }

    public void setLastLockTime(Timestamp lastLockTime) {
        this.lastLockTime = lastLockTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    //    Way too advanced
//    public List<Version> getPreviousVersions() {
//        throw new UnsupportedOperationException("Not implemented yet");
//    }

    /****************************************************************************************************************/

    public Paragraph() {
        this.state = ParagraphState.INITIAL;
    }

    public Paragraph(String title) {
        this();
        this.title = title;
    }

    public Paragraph(String title, String content) {
        this(title);
        this.content = content;
    }

    public void lockFor(User user) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public void updateContent(String content) {
        this.content = content;
        state = ParagraphState.PENDING;
    }

    public void validate() {
        state = ParagraphState.VALIDATED;
    }

    public void reject() {
        state = ParagraphState.REJECTED;
    }

    public void assign(User user) {
        assignees.add(user);
    }

    public void assign(List<User> users) {
        assignees.clear();
        assignees.addAll(users);
    }

    public Set<User> getAssignees() {
        return assignees;
    }

    public static Finder<Long, Paragraph> find = new Finder<>(
            Long.class, Paragraph.class
    );
}
