package models;

import com.avaje.ebean.Ebean;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Represent a volume of a book
 * @author Sonia
 *
 */
@Entity
@Table(name="volumes")
public class Volume extends Model implements IAssignable {
    /**
     * Id of the volume
     */
	@Id
    @Constraints.Min(10)
    @GeneratedValue
    private Long id;

	/**
	 * Title of the volume
	 */
    private String title;

    /**
     * Book who contains the volume
     */
    @ManyToOne
    private Book book; // Parent : book who contains the current volume

    /**
     * Chapters contained in this volume
     */
    @OneToMany(mappedBy = "volume", cascade  = CascadeType.ALL)
    private List<Chapter> chapters; // Children

    /**
     * Groups allowed to access the volume
     */
    @ManyToMany
    private List<Group> groups; // Groups allowed to access this volume

    /****************************************************************************************************************/

    /**
     * Returns the id of the volume
     * @return	volume's id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id of the volume
     * @param id	volume's id
     */
    public void setId(Long id) {
    	Objects.requireNonNull(id);
        this.id = id;
    }

    /**
     * Returns the title of the volume
     * @return	the volume's title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of the volume
     * @param title	the volume's title
     * @throws Exception	if the title is empty
     */
    public void setTitle(String title) throws Exception {
    	Objects.requireNonNull(title);
        if(title.isEmpty()){
            throw new Exception("Title must be set");
        }
        this.title = title;
        Ebean.update(this);
    }

    /**
     * Returns the chapters of the volume
     * @return	the list of chapters associated with the volume
     */
    public List<Chapter> getChapters() {
        chapters.sort(new Comparator<Chapter>() {
            @Override
            public int compare(Chapter o1, Chapter o2) {
                Long o1id = o1.getId();
                Long o2id = o2.getId();
                if(o1id == null) { o1id = 0L ; }
                if(o2id == null) { o2id = 0L ; }
                return Long.compare(o1id, o2id);
            }
        });
        return chapters;
    }

    /**
     * Sets the chapters of the volume
     * @param chapters	the list of chapter to associate with the volume
     */
    public void setChapters(List<Chapter> chapters) {
    	Objects.requireNonNull(chapters);
        this.chapters = chapters;
    }

    /**
     * Returns the book parent of the volume
     * @return	the book parent of the volume
     */
    public Book getBook() {
        return book;
    }

    /**
     * Sets the book parent of the volume
     * @param book	the book to associate with the volume
     */
    public void setBook(Book book) {
    	Objects.requireNonNull(book);
        this.book = book;
    }

    /**
     * Returns the groups allowed to access the volume
     * @return	a list of groups allowed to access the volume
     */
    public List<Group> getGroups() {
        return groups;
    }

    /**
     * Sets the groups allowed to access the volume
     * @param groups	a list of groups allowed to access the volume
     */
    public void setGroups(List<Group> groups) {
    	Objects.requireNonNull(groups);
        this.groups = groups;
    }

    /****************************************************************************************************************/

    /**
     * Empty constructor for the volume
     */
    protected Volume() {
        chapters = new ArrayList<>();
        groups = new ArrayList<>();
    }

    /**
     * Constructor for the volume
     * @param title	the title of the volume
     */
    public Volume(String title) {
        this();
        this.title = title;
    }

    /**
     * Attach a chapter to the volume
     * @param chapter	the chapter to attach
     */
    public void attach(Chapter chapter) {
        Objects.requireNonNull(chapter);
        this.chapters.add(chapter);
    }

    /**
     * Assign a user to the volume if the user is in a group allowed
     */
    @Override
    public void assign(User user) {
    	throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Assign users to the volume if they are in a group allowed
     */
    @Override
    public void assign(List<User> user) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Returns the list of users assigned to the volume
     */
    @Override
    public List<User> getAssignees() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * The finder for requests in the dataBase.
     */
    public static Finder<Long, Volume> find = new Finder<>(
            Long.class, Volume.class
    );

    public int getCompletionPercentage() {
        int total = 0 ;
        int cumulated = 0;
        for(Chapter c : chapters) {
            cumulated += c.getCompletionPercentage();
            total++ ;
        }
        if(cumulated == 0 || total == 0) {
            return 0;
        }
        return cumulated / total ;
    }
}