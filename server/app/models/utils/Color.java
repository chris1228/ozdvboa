package models.utils;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "colors")
public class Color extends Model {
    @Id
    @Constraints.Min(10)
    private Long id;

    private int alpha;
    private int r, g, b;

    /****************************************************************************************************************/

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    /****************************************************************************************************************/

    public Color() {
        alpha = r = g = b = 255 ;
    }

    public Color(int r, int g, int b, int alpha) {
        this.r = (r < 0) ? 0 : (r > 255) ? 255 : r ;
        this.g = (g < 0) ? 0 : (g > 255) ? 255 : g ;
        this.b = (b < 0) ? 0 : (b > 255) ? 255 : b ;
        this.alpha = (alpha < 0) ? 0 : (alpha > 255) ? 255 : alpha ;
    }

    public static Finder<Long, Color> find = new Finder<>(
            Long.class, Color.class
    );
}
