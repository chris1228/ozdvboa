package models.utils;

import models.Chapter;
import models.Paragraph;
import models.Volume;

import java.util.ArrayList;
import java.util.List;

/**
 * Don't bother putting that class in the database, it's just for testing
 */
public class Mock {

    public static List<Volume> getSampleVolumes() {
        List<Volume> volumes = new ArrayList<>();
        for(int i = 0; i < 10; i++) {
            Volume v = new Volume("Volume " + i);
            for(int j = 0; j < 5 ; j++) {
                Chapter c = null;
                try { c = new Chapter("Chapter " + j);
                } catch (Exception ignored) { }
                for(int k = 0 ; k < 5 ; k++) {
                    Paragraph p = new Paragraph("Paragraph " + k, "abcdefg") ;
                    c.attach(p);
                }
                v.attach(c);
            }
            volumes.add(v);
        }
        return volumes;
    }
}
