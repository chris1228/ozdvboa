package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "chapters")
public class Chapter extends Model implements IAssignable {
    @Id
    @Constraints.Min(10)
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Volume volume; // Parent : volume which contains the current chapter

    @OneToMany(mappedBy = "chapter", cascade = CascadeType.ALL)
    private List<Paragraph> paragraphs; // Children

    @ManyToMany
    private List<Group> groups; // Groups allowed to access this chapter

    private String title;

    /****************************************************************************************************************/

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) throws Exception {
        Objects.requireNonNull(title);
        if(title.isEmpty()){
            throw new Exception("Title must be set");
        }
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Volume getVolume() {
        return volume;
    }

    public void setVolume(Volume volume) {
        this.volume = volume;
    }

    public List<Paragraph> getParagraphs() {
        paragraphs.sort(new Comparator<Paragraph>() {
            @Override
            public int compare(Paragraph o1, Paragraph o2) {
                Long o1id = o1.getId();
                Long o2id = o2.getId();
                if(o1id == null) { o1id = 0L ; }
                if(o2id == null) { o2id = 0L ; }
                return Long.compare(o1id, o2id);
            }
        });
        return paragraphs;
    }

    public void setParagraphs(List<Paragraph> paragraphs) {
        this.paragraphs = paragraphs;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    /****************************************************************************************************************/

    protected Chapter() {
        paragraphs = new ArrayList<>();
        groups = new ArrayList<>();
    }

    public Chapter(String title) throws Exception {
        this();
        this.setTitle(title);
    }

    public void attach(Paragraph paragraph) {
        Objects.requireNonNull(paragraph);
        this.paragraphs.add(paragraph);
    }

    @Override
    public void assign(User user) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public List<User> getAssignees() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void assign(List<User> users) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static Finder<Long, Chapter> find = new Finder<>(
            Long.class, Chapter.class
    );

    public int getCompletionPercentage() {
        int total = 0 ;
        int done = 0;
        for(Paragraph p : paragraphs) {
            if(p.getState().equals(ParagraphState.VALIDATED)) {
                done++;
            }
            total++ ;
        }

        if(done == 0 || total == 0) {
            return 0 ;
        } else {
            return (int) (done * 100.0/total);
        }
    }
}
