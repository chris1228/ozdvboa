package models

import javax.persistence._

import play.db.ebean.Model
import securesocial.core.{PasswordInfo, OAuth2Info, OAuth1Info, AuthenticationMethod}

@Entity
@Table(name="basic_profile")
@Embeddable
class MyBasicProfile(
  @Column val providerId: String,
  @Column val userId: String,
  @Column val firstName: String,
  @Column val lastName: String,
  @Column val fullName: String,
  @Column val email: String,
  @Column val avatarUrl: String,
  @Column val authMethod: AuthenticationMethod,
  @Column val oAuth1Token: String,
  @Column val oAuth1Secret: String,
  @Column val oAuth2AccessToken: String,
  @Column val oAuth2TokenType: String,
  @Column val oAuth2ExpiresIn: Int,
  @Column val oAuth2RefreshToken: String,
  @Column val passwordInfoHasher: String,
  @Column val passwordInfoPassword: String,
  @Column val passwordInfoSalt: String) extends Model {

  @Id
  @GeneratedValue
  val id: Long = null.asInstanceOf[Long]

  def this(basicProfile: securesocial.core.BasicProfile) = {
    this(
      basicProfile.providerId,
      basicProfile.userId,
      basicProfile.firstName.orNull,
      basicProfile.lastName.orNull,
      basicProfile.fullName.orNull,
      basicProfile.email.orNull,
      basicProfile.avatarUrl.orNull,
      basicProfile.authMethod,
      basicProfile.oAuth1Info.map(_.token).orNull,
      basicProfile.oAuth1Info.map(_.secret).orNull,
      basicProfile.oAuth2Info.map(_.accessToken).orNull,
      basicProfile.oAuth2Info.flatMap(_.tokenType).orNull,
      basicProfile.oAuth2Info.flatMap(_.expiresIn).getOrElse(null.asInstanceOf[Int]),
      basicProfile.oAuth2Info.flatMap(_.refreshToken).orNull,
      basicProfile.passwordInfo.map(_.hasher).orNull,
      basicProfile.passwordInfo.map(_.password).orNull,
      basicProfile.passwordInfo.flatMap(_.salt).orNull
    )
  }

  def toSSBasicProfile: securesocial.core.BasicProfile = {
    new securesocial.core.BasicProfile(
      providerId,
      userId,
      Option(firstName),
      Option(lastName),
      Option(fullName),
      Option(email),
      Option(avatarUrl),
      authMethod,
      Option(new OAuth1Info(oAuth1Token, oAuth1Secret)),
      Option(new OAuth2Info(oAuth2AccessToken, Option(oAuth2TokenType), Option(oAuth2ExpiresIn), Option(oAuth2RefreshToken))),
      Option(new PasswordInfo(passwordInfoHasher, passwordInfoPassword, Option(passwordInfoSalt)))
    )
  }
}
