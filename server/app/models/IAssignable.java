package models;

import java.util.List;

public interface IAssignable {
    public void assign(User user);
    public List<User> getAssignees();
    public void assign(List<User> users);
}
