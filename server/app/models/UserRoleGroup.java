package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Objects;

/**
 * Represents the ternary relation User-Role-Group
 * A user can have one or more roles given a group
 * and he can belong to several groups at the same time.
 **/

@Entity
@Table(name = "userrolegroup")
public class UserRoleGroup extends Model {
    @Id
    @Constraints.Min(10)
    @GeneratedValue
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Group group;

    @Enumerated(EnumType.STRING)
    private Roles role;

    /****************************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    /****************************************************************************************************************/

    protected UserRoleGroup () {}

    public UserRoleGroup(User user, Group group, Roles role) {
        Objects.requireNonNull(user);
        Objects.requireNonNull(group);
        Objects.requireNonNull(role);
        this.user = user;
        this.group = group;
        this.role = role;
    }

    public static Finder<Long, UserRoleGroup> find = new Finder<>(
            Long.class, UserRoleGroup.class
    );
}
