package models;

import models.utils.Color;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

public class Settings extends Model {
    public Long id;

    private Color color;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void changeDefaultColor(Color color) {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}