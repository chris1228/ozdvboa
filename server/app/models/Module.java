package models;

/**
 * Represents the modules on the sidebar
 * Don't bother persisting that in the database
 */
public enum Module {
    TASKS,
    NAVIGATION,
    BOOK,
    USERS
}
