package models;

public class ParagraphState {
    public final static String INITIAL = "INITIAL";
    public final static String PENDING = "PENDING";
    public final static String REJECTED = "REJECTED";
    public final static String VALIDATED = "VALIDATED";
}
