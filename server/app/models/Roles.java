package models;

public enum Roles {
    ADMIN, // Book edit
    VOLUME, // Volume edit
    CHAPTER, // Chapter edit
    ENGINEER, // Paragraph edit,
    CLIENT
}
