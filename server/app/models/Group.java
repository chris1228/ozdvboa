package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "edtcgroups")
public class Group extends Model {
    @Id
    @Constraints.Min(10)
    @GeneratedValue
    private Long id;

    @ManyToMany
    private List<Volume> volumes; // Volumes accessible by this group

    @ManyToMany
    private List<Chapter> chapters; // Chapters accessible by this group

    @ManyToMany
    private List<Paragraph> paragraphs; // Paragraphs accessible by this group

    private String name;

    /****************************************************************************************************************/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Volume> getVolumes() {
        return volumes;
    }

    public void setVolumes(List<Volume> volumes) {
        this.volumes = volumes;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    public List<Paragraph> getParagraphs() {
        return paragraphs;
    }

    public void setParagraphs(List<Paragraph> paragraphs) {
        this.paragraphs = paragraphs;
    }

    /****************************************************************************************************************/

    protected Group() {
        volumes = new ArrayList<>();
        chapters = new ArrayList<>();
        paragraphs = new ArrayList<>();
    }

    public Group(String name) {
        this();
        this.name = name;
    }

    public void addUser(User user) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public void removeUser(User user) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static Finder<Long, Group> find = new Finder<>(
            Long.class, Group.class
    );
}
