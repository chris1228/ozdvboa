package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "edtcusers")
public class User extends Model {
    @Id
    @Constraints.Min(10)
    @GeneratedValue
    private Long id;

    private boolean admin;

    private Settings settings;

    @Embedded
    private MyBasicProfile basicProfile;

    public User() {
        admin = true ;
    }

    public User(MyBasicProfile basicProfile) {
        this.basicProfile = basicProfile;
    }

    /****************************************************************************************************************/

    public String getFirstName() {
        return basicProfile.firstName();
    }

    public void setFirstName(String firstName) {
        this.basicProfile = new MyBasicProfile(
                this.basicProfile.providerId(),
                this.basicProfile.userId(),
                firstName,
                this.basicProfile.lastName(),
                this.basicProfile.fullName(),
                this.basicProfile.email(),
                this.basicProfile.avatarUrl(),
                this.basicProfile.authMethod(),
                this.basicProfile.oAuth1Token(),
                this.basicProfile.oAuth1Secret(),
                this.basicProfile.oAuth2AccessToken(),
                this.basicProfile.oAuth2TokenType(),
                this.basicProfile.oAuth2ExpiresIn(),
                this.basicProfile.oAuth2RefreshToken(),
                this.basicProfile.passwordInfoHasher(),
                this.basicProfile.passwordInfoPassword(),
                this.basicProfile.passwordInfoSalt()
        );
    }

    public String getLastName() {
        return basicProfile.lastName();
    }

    public void setLastName(String lastName) {
        this.basicProfile = new MyBasicProfile(
                this.basicProfile.providerId(),
                this.basicProfile.userId(),
                this.basicProfile.firstName(),
                lastName,
                this.basicProfile.fullName(),
                this.basicProfile.email(),
                this.basicProfile.avatarUrl(),
                this.basicProfile.authMethod(),
                this.basicProfile.oAuth1Token(),
                this.basicProfile.oAuth1Secret(),
                this.basicProfile.oAuth2AccessToken(),
                this.basicProfile.oAuth2TokenType(),
                this.basicProfile.oAuth2ExpiresIn(),
                this.basicProfile.oAuth2RefreshToken(),
                this.basicProfile.passwordInfoHasher(),
                this.basicProfile.passwordInfoPassword(),
                this.basicProfile.passwordInfoSalt()
        );
    }

    public String getEmail() {
        return basicProfile.email();
    }

    public void setEmail(String email) {
        this.basicProfile = new MyBasicProfile(
                this.basicProfile.providerId(),
                this.basicProfile.userId(),
                this.basicProfile.firstName(),
                this.basicProfile.lastName(),
                this.basicProfile.fullName(),
                email,
                this.basicProfile.avatarUrl(),
                this.basicProfile.authMethod(),
                this.basicProfile.oAuth1Token(),
                this.basicProfile.oAuth1Secret(),
                this.basicProfile.oAuth2AccessToken(),
                this.basicProfile.oAuth2TokenType(),
                this.basicProfile.oAuth2ExpiresIn(),
                this.basicProfile.oAuth2RefreshToken(),
                this.basicProfile.passwordInfoHasher(),
                this.basicProfile.passwordInfoPassword(),
                this.basicProfile.passwordInfoSalt()
        );
    }

    public void setBasicProfile(MyBasicProfile basicProfile) {
        this.basicProfile = basicProfile;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Paragraph> getParagraphs() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public List<Chapter> getChapters() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public List<Volume> getVolumes() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public boolean isAdmin() {
        // SUPER HACK
        return basicProfile.email().equals("admin");
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    /****************************************************************************************************************/

    public void addToGroupWithRole(Group group, Roles role) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public void removeRoleFromGroup(Group group, Roles role) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Removes the user from a group completely. Every role he had in this group disappears.
     * @param group The group from which the user is removed
     */
    public void removeFromGroup(Group group) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static Finder<Long, User> find = new Finder<>(
            Long.class, User.class
    );

    public MyBasicProfile getBasicProfile() {
        return basicProfile;
    }
}
