package services;

import com.avaje.ebean.Ebean;
import models.Book;
import models.Chapter;

import java.util.List;
import java.util.Objects;

/**
 * Service for Chapter
 * Allow user to insert upate, delete and search for chapter
 * Created by Cedric Baulamon on 04/02/15.
 */

public class ChapterService {
    /***
     * insert  the Chapter into the Database
     * @param chapter
     * @throws Exception if id is not null
     */
    public static void insert(Chapter chapter) throws Exception {
        Objects.requireNonNull(chapter);
        if(chapter.getId() != null){
            throw new Exception("Id must be Empty");
        }
        Ebean.save(chapter);
    }

    /***
     * delete  the Chapter into the Database
     * @param chapter
     * @throws Exception if chapter is null or id is null
     */
    public static void delete(Chapter chapter) throws Exception {
        Objects.requireNonNull(chapter);
        if(chapter.getId() == null){
            throw new Exception("Id must be set");
        }
        Ebean.delete(chapter);
    }


    /***
     * update the chapter into the Database
     * @param chapter
     * @throws Exception if chapter is null or id is null
     */
    public static void update(Chapter chapter) throws Exception {
        Objects.requireNonNull(chapter);
        if(chapter.getId() == null){
            throw new Exception("Id must be set");
        }
        Ebean.update(chapter);
    }

    /***
     * find by id a chapter on the Database
     * @param id
     * @return  a chapter if exist or false
     */
    public static Chapter findById(long id){
        Objects.requireNonNull(id);
        return Chapter.find.byId(id);
    }

    /***
     * find by title a chapter on the Database
     * @param title
     * @return all chapter which contains the given title
     */
    public static List<Chapter> findByTitle(String title){
        Objects.requireNonNull(title);
        return Chapter.find.where()
                .like("title", "%"+title+"%")
                .orderBy("id")
                .findList();
    }

    /***
     * Search all chapter on Database
     * @return all chapter on Database
     */
    public static List<Chapter>findAll(){
        return Chapter.find.all();
    }

}
