package services;

import com.avaje.ebean.Ebean;
import exceptions.NotImplementedException;
import models.MyBasicProfile;
import models.MyToken;
import models.User;
import org.mindrot.jbcrypt.BCrypt;
import play.libs.F.Promise;
import scala.Option;
import scala.Some;
import securesocial.core.AuthenticationMethod;
import securesocial.core.BasicProfile;
import securesocial.core.PasswordInfo;
import securesocial.core.java.BaseUserService;
import securesocial.core.java.Token;
import securesocial.core.services.SaveMode;

import java.util.List;

public class MyUserService extends BaseUserService<User> {
    @Override
    public Promise<User> doSave(BasicProfile basicProfile, SaveMode saveMode) {
        User user = new User(new MyBasicProfile(basicProfile));

        if (saveMode.equals(SaveMode.SignUp())) {
            Ebean.save(user);
        } else if (saveMode.equals(SaveMode.PasswordChange())) {
            user.setBasicProfile(new MyBasicProfile(basicProfile));
            Ebean.update(user);
        }

        return Promise.pure(user);
    }

    public static void createNewUser(String email, String password, String firstName, String lastName) {
        SaveMode sv = SaveMode.SignUp();
        String s = BCrypt.gensalt();
        BasicProfile bp = new BasicProfile(
                "userpass",
                email,
                Option.apply(firstName),
                Option.apply(lastName),
                Option.apply(firstName + " " + lastName),
                Option.apply(email),
                Option.empty(),
                AuthenticationMethod.UserPassword(),
                Option.empty(),
                Option.empty(),
                Option.apply(new PasswordInfo("bcrypt", BCrypt.hashpw(password, s), Option.empty()))
        );
        User usr = new User(new MyBasicProfile(bp));
        Ebean.save(usr);
    }

    @Override
    public Promise<Token> doSaveToken(Token token) {
        Ebean.save(new MyToken(token));
        return Promise.pure(null);
    }

    @Override
    public Promise<User> doLink(User user, BasicProfile basicProfile) {
        throw new NotImplementedException();
    }

    @Override
    public Promise<BasicProfile> doFind(String providerId, String userId) {
        List<User> users = User.find
                .where()
                .eq("email", userId)
                .findList();

        if (users.size() == 0) {
            return Promise.pure(null);
        } else {
            return Promise.pure(users.get(0).getBasicProfile().toSSBasicProfile());
        }
    }

    public static List<User> findAll() {
        return User.find.all();
    }

    public static User findById(long id) {
        return User.find.byId(id);
    }

    public static User findByLogin(String login) {
        return User.find
                .where()
                .eq("email", login)
                .findUnique();
    }

    @Override
    public Promise<PasswordInfo> doPasswordInfoFor(User user) {
        throw new NotImplementedException();
    }

    @Override
    public Promise<BasicProfile> doUpdatePasswordInfo(User user, PasswordInfo passwordInfo) {
        throw new NotImplementedException();
    }

    @Override
    public Promise<Token> doFindToken(String tokenId) {
        MyToken token = MyToken.find.where()
                .ilike("uuid", tokenId)
                .findUnique();

        return Promise.pure(token.toSSToken());
    }

    @Override
    public Promise<BasicProfile> doFindByEmailAndProvider(String email, String providerId) {
        return doFind(providerId, email);
    }

    @Override
    public Promise<Token> doDeleteToken(String uuid) {
        MyToken token = MyToken.find.where().eq("uuid", uuid).findUnique();
        Ebean.delete(token);

        return Promise.pure(token.toSSToken());
    }

    @Override
    public void doDeleteExpiredTokens() {
    }
}