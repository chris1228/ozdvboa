package services;

import com.avaje.ebean.Ebean;
import models.Book;
import models.Volume;

import java.util.List;
import java.util.Objects;

/**
 * Service for Volume
 * Allow user to insert update, delete and search for volume
 * @author Sonia
 * 
 */
public class VolumeService {
    /**
     * insert the volume into the database
     * @param volume vol to insert
     * @throws Exception	if id is not empty
     */
    public static void insert(Volume volume) {
        Objects.requireNonNull(volume);
        if(volume.getId() != null){
            throw new RuntimeException("Id must be Empty");
        }
        Ebean.save(volume);
    }

    /**
     * delete the volume of the database
     * @param volume	the volume to delete
     * @throws Exception 	if the volume is null or id is null
     */
    public static void delete(Volume volume) throws Exception {
        Objects.requireNonNull(volume);
        if(volume.getId() == null){
            throw new Exception("Id must be set");
        }
        Ebean.delete(volume);
    }


    /**
     * update the volume in the database
     * @param volume	the volume to update
     * @throws Exception	if the volume is null or id is null
     */
    public static void update(Volume volume) throws Exception {
        Objects.requireNonNull(volume);
        if(volume.getId() == null){
            throw new Exception("Id must be set");
        }
        Ebean.update(volume);
    }

    /**
     * Search all volume in the database
     * @return 	the volumes in the database
     */
    public static List<Volume>findAll(){
        return Volume.find.all();
    }
    
    /**
     * find a volume by id in the database
     * @param id	the id of the volume
     * @return	the volume if it exist or false
     */
    public static Volume findById(long id){
        Objects.requireNonNull(id);
        return Volume.find.byId(id);
    }

    /**
     * find a volume by title in the database
     * @param title	the id of the volume
     * @return 	the volumes which contains the title
     */
    public static List<Volume> findByTitle(String title){
        Objects.requireNonNull(title);
        return Volume.find.where()
                .like("title", "%"+title+"%")
                .orderBy("id")
                .findList();
    }

}
