package services;

import com.avaje.ebean.Ebean;
import java.util.List;
import models.Book;

/**
 * Service for Book.
 * Allow user to insert, update, delete and search for book.
 * @author Olivier Norture <olivier@norture.fr>
 */
public class BookService {
    /**
     * Insert the book into the DataBase.
     * Book's id must be empty.
     * @param book the book to insert.
     * @throws Exception if the book's id isn't empty or if a book already exist.
     */
    public static void insert(Book book) throws Exception{
        if(book.getId() != null){
            throw new Exception("Id must be empty");
        }
        //Check if a book already exist
        if(BookService.getAllBook().size() > 0){
            throw new Exception("A book already exist");
        }
        Ebean.save(book);
    }
    
    /**
     * Delete the book into the DataBase.
     * Book's id must be set.
     * @param book the book to delete.
     * @throws Exception if the book's id is not set.
     */
    public static void delete(Book book) throws Exception{
        if(book.getId() == null){
            throw new Exception("Id must be set");
        }
        Ebean.delete(book);
    }
    
    /**
     * Update the book into the DataBase.
     * Book's id must be set.
     * @param book the book to update.
     * @throws Exception if the book's id is not set.
     */
    public static void update(Book book) throws Exception{
        if(book.getId() == null){
            throw new Exception("Id must be set");
        }
        Ebean.update(book);
    }
    
    /**
     * Get the Book of the application.
     * @return the book of the application.
     */
    public static Book getBook(){
        return BookService.getAllBook().isEmpty() ? null : BookService.getAllBook().get(0);
    }
    
//    /**
//     * Search a book in the DataBase with an id.
//     * @param id the book's id to find.
//     * @return a book if the id exist, otherwise false.
//     */
//    public static Book findById(long id){
//        Objects.requireNonNull(id);
//        return Book.find.byId(id);
//    }
//    
//    /**
//     * Search books in the DataBase by title.
//     * @param title the book's BookService.getAllBook().size()title to find
//     * @return all books which contains the given title.
//     */
//    public static List<Book> findByTitle(String title){
//        Objects.requireNonNull(title);
//        return Book.find.where()
//                .ilike("title", "%" + title + "%")
//                .orderBy("id asc")
//                .findList();
//    }
//    
    /**
     * Return all books in the DataBase.
     * @return all books in the DataBase.
     */
    private static List<Book> getAllBook(){
        return Book.find.all();
    }
}
