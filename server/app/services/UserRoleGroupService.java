package services;

import models.Group;
import models.Roles;
import models.User;

import java.util.List;
import java.util.Map;

public class UserRoleGroupService {

    public static void insert(UserRoleGroupService relation) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static void update(UserRoleGroupService relation) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static void delete(UserRoleGroupService relation) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static UserRoleGroupService findById(long id){
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static List<User> findUsers(Group g) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static Map<User,List<Roles>> findUsersAndRoles(Group g) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static List<Group> findGroups(User u) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static List<Roles> findRoles(User u) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public static Map<Group, List<Roles>> findGroupAndRoles(User u) {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
