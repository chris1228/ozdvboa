package services

import models.User
import securesocial.core.RuntimeEnvironment
import securesocial.core.providers.UsernamePasswordProvider
import securesocial.core.services.UserService

import scala.collection.immutable.ListMap

class MyRuntimeEnvironment extends RuntimeEnvironment.Default[User] {
  override val userService: UserService[User] = new MyUserService()
  override lazy val providers = ListMap(
    include(
      new UsernamePasswordProvider[User](userService, avatarService, viewTemplates, passwordHashers)
    )
  )
}