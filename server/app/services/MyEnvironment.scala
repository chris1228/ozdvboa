package services

import models.User
import securesocial.core.RuntimeEnvironment
import securesocial.core.services.UserService

class MyEnvironment extends RuntimeEnvironment.Default[User] {
  override val userService: UserService[User] = new MyUserService()
}