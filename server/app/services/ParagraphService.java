package services;

import models.Paragraph;
import models.User;

import java.util.List;

public class ParagraphService {
    public static List<Paragraph> findByUser(User u) {
        return Paragraph.find.select("*")
                .fetch("assignees")
                .where()
                .eq("assignees.id", u.getId())
                .findList();
    }
}
