import models.Book;
import play.Application;
import play.GlobalSettings;
import securesocial.core.RuntimeEnvironment;
import services.BookService;
import services.MyRuntimeEnvironment;
import services.MyUserService;

public class Global extends GlobalSettings {
    private RuntimeEnvironment env = new MyRuntimeEnvironment();

    @Override
    public <A> A getControllerInstance(Class<A> controllerClass) throws Exception {
        A result;
        try {
            result = controllerClass.getDeclaredConstructor(RuntimeEnvironment.class).newInstance(env);
        } catch (NoSuchMethodException e) {
            result = super.getControllerInstance(controllerClass);
        }
        return result;
    }

    @Override
    public void onStart(Application app) {
        // Create the first (and last) book with a default title
        Book initialBook = new Book("Default Book title");
        try {
            if(MyUserService.findByLogin("admin") == null) {
                MyUserService.createNewUser("admin","admin", "Antoine", "Célier");
            }
            BookService.insert(initialBook);
        } catch (Exception e) {
            play.Logger.error("onStart init book error : " + e);
            // Fail silently, we already have a book
        }
    }
}