package controllers;

import java.util.Map;
import java.util.Objects;
import models.Book;
import models.Module;
import models.User;
import play.mvc.Http;
import play.mvc.Result;
import securesocial.core.java.SecureSocial;
import securesocial.core.java.SecuredAction;
import services.BookService;

import static play.mvc.Controller.ctx;
import static play.mvc.Controller.request;
import static play.mvc.Results.badRequest;
import static play.mvc.Results.internalServerError;
import static play.mvc.Results.ok;

/**
 * Book controller, persist data in the DataBase.
 * @author Olivier Norture <olivier@norture.fr>
 */
public class BookController {

    /**
     * Change the book's title in the database.
     * @throws Exception if title is null or empty.
     */
    @SecuredAction
    public Result changeTitle(){
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        // Get the 'title' from HTTP body
        Http.RequestBody body = request().body();
        Map<String, String[]> formResult = body.asFormUrlEncoded();
        String[] aux = formResult.get("title") ;
        if(aux == null || aux.length < 1 || aux[0].isEmpty()) {
            return badRequest();
        }
        String title = aux[0] ;

        // Retrieve book from database
        Book b = BookService.getBook();

        // Change its title
        try {
            b.setTitle(title);
            // Persist
            BookService.update(b);
        } catch (Exception e) {
            play.Logger.error("Change title error " + e);
            return internalServerError(e.toString());
        }
        return ok(views.html.book.render(currentUser, Module.BOOK, b));
    }

    @SecuredAction
    public Result configure() {
        Book currentBook = BookService.getBook();
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        return ok(views.html.book.render(currentUser, Module.BOOK, currentBook));
    }
}
