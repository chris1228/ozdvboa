package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.node.ObjectNode;
import exceptions.NotImplementedException;
import models.Book;
import models.Chapter;
import models.Module;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import securesocial.core.java.SecureSocial;
import services.BookService;
import services.ChapterService;
import services.MyUserService;
import views.html.*;
import securesocial.core.java.SecuredAction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Paragraph extends Controller {
    @SecuredAction
    public Result list() {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        List<models.Paragraph> paragraphs = models.Paragraph.find.all();

        return ok(views.html.paragraph.list.render(currentUser, paragraphs));
    }

    @SecuredAction
    public Result newGet() {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        return ok(views.html.paragraph.new_.render(currentUser));
    }

    @SecuredAction
    public Result newPost() {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        DynamicForm form = Form.form().bindFromRequest();
        String title = form.get("paragraph-title");
        String chapterId = form.get("chapter-id");
        models.Paragraph p = new models.Paragraph(title);
        Chapter c = null;
        Book b = null;
        try {
            c = ChapterService.findById(Long.parseLong(chapterId));
            b = BookService.getBook();
        } catch (Exception e) {
            play.Logger.error("Couldn't post new paragraph " + e);
            return badRequest(views.html.book.render(currentUser, Module.BOOK, b));
        }
        p.setChapter(c);
        Ebean.save(p);
        return redirect(controllers.routes.Paragraph.show(p.getId()));
    }

    @SecuredAction
    public Result show(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        models.Paragraph paragraph = models.Paragraph.find.byId(id);
        List<User> users = MyUserService.findAll();
        return ok(views.html.paragraph.show.render(currentUser, paragraph, users));
    }

    @SecuredAction
    public Result editGet(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        models.Paragraph paragraph = models.Paragraph.find.byId(id);
        return ok(views.html.paragraph.edit.render(currentUser, paragraph));
    }

    @SecuredAction
    public Result editPost(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        List<User> users = MyUserService.findAll();
        DynamicForm form = Form.form().bindFromRequest();
        String content = form.get("paragraph-content");
        models.Paragraph paragraph = models.Paragraph.find.byId(id);

        if(paragraph == null) {
            return badRequest();
        }
        if (content == null) {
            return badRequest(views.html.paragraph.show.render(currentUser, paragraph, users));
        }

        if (!content.isEmpty()) {
            paragraph.updateContent(content);
            Ebean.update(paragraph);
        }

        return ok(views.html.paragraph.show.render(currentUser, paragraph, users));
    }

    @SecuredAction
    public Result editAdmin(Long id) {
        List<User> users = MyUserService.findAll();
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        DynamicForm form = Form.form().bindFromRequest();
        String content = form.get("paragraph-content");
        String title = form.get("title");

        List<User> responsibles = new ArrayList<>();

        String[] ids = request().body().asFormUrlEncoded().get("responsibles");
        if(ids != null) {
            for (String respId : ids) {
                try {
                    Long parsedId = Long.parseLong(respId);
                    User u = MyUserService.findById(parsedId);
                    if (u != null) {
                        responsibles.add(u);
                    }
                } catch (NumberFormatException ignored) {
                }
            }
        }

        models.Paragraph paragraph = models.Paragraph.find.byId(id);
        if(content != null && !content.isEmpty() && !content.equals(paragraph.getContent())) {
            paragraph.setContent(content);
        }
        paragraph.assign(responsibles);

        if(title != null && !title.isEmpty()) {
            paragraph.setTitle(title);
        }

        Ebean.update(paragraph);
        Ebean.refreshMany(paragraph, "assignees");

        return ok(views.html.paragraph.show.render(currentUser, paragraph, users));
    }

    @SecuredAction
    public Result delete(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        models.Paragraph paragraph = models.Paragraph.find.byId(id);
        try { Ebean.delete(paragraph); } catch (NullPointerException ex) {}

        return redirect(controllers.routes.Paragraph.list());
    }

    @SecuredAction
    public Result validate(Long id) {
        models.Paragraph paragraph = models.Paragraph.find.byId(id);
        paragraph.validate();
        Ebean.update(paragraph);

        ObjectNode result = Json.newObject();
        return ok(result);
    }

    @SecuredAction
    public Result reject(Long id) {
        models.Paragraph paragraph = models.Paragraph.find.byId(id);
        paragraph.reject();
        Ebean.update(paragraph);

        ObjectNode result = Json.newObject();
        return ok(result);
    }
}
