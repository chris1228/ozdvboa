package controllers;

import models.*;
import models.Paragraph;
import play.mvc.*;
import securesocial.core.java.SecureSocial;
import securesocial.core.services.UserService;
import services.BookService;
import services.MyUserService;
import services.ParagraphService;
import views.html.*;
import securesocial.core.RuntimeEnvironment;
import securesocial.core.java.SecuredAction;

import java.util.*;

public class Application extends Controller {
    private RuntimeEnvironment<User> env;

    public Application(RuntimeEnvironment<User> env) {
        this.env = env;
    }

    public Result login() {
        return ok(login.render());
    }

    public Result test() {
        return ok("test okay");
    }

    @SecuredAction
    public Result dashboard() {
        User tmp = (User) ctx().args.get(SecureSocial.USER_KEY);
        User currentUser = MyUserService.findByLogin(tmp.getEmail());

        List<Paragraph> paragraphs = ParagraphService.findByUser(currentUser);
        Set<Volume> concernedVolumes = new HashSet<>();
        for(Paragraph p : paragraphs) {
            concernedVolumes.add(p.getChapter().getVolume());
        }

        // Real function
        List<Volume> volumes = new ArrayList<>(concernedVolumes);

        return ok(dashboard.render(currentUser,
                scala.collection.JavaConversions.asScalaBuffer(paragraphs),
                scala.collection.JavaConversions.asScalaBuffer(volumes), Module.TASKS));
    }

    @SecuredAction
    public Result navigation() {
        // Real function
        List<Volume> volumes = BookService.getBook().getVolumes();

        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        return ok(navigation.render(
                currentUser,
                scala.collection.JavaConversions.asScalaBuffer(volumes),
                Module.NAVIGATION));
    }
}
