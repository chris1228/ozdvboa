package controllers;

import com.avaje.ebean.Ebean;

import exceptions.NotImplementedException;
import models.Book;
import models.Module;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.*;
import securesocial.core.java.SecureSocial;
import services.VolumeService;
import views.html.*;
import securesocial.core.java.SecuredAction;
import services.BookService;

import java.util.List;

public class VolumeController extends Controller {
	@SecuredAction
	public Result list() {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

		List<models.Volume> volumes = models.Volume.find.all();
		return ok(views.html.volume.list.render(currentUser, volumes));
	}

    @SecuredAction
    public Result newGet() {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        return ok(views.html.volume.new_.render(currentUser, Module.BOOK));
    }

    @SecuredAction
    public Result newPost() {
        DynamicForm form = Form.form().bindFromRequest();
        String title = form.get("title");
        if(title == null) {
            return badRequest();
        }
		models.Volume v = new models.Volume(title);
        models.Book b = BookService.getBook();
        v.setBook(b);
        VolumeService.insert(v);

		b.attach(v);
        Ebean.update(b);
        play.Logger.debug("Book has now " + b.getVolumes().size() + " volumes");
        long id = v.getId();

        Book test = BookService.getBook();
        play.Logger.debug("Refetched book has now " + test.getVolumes().size() + " volumes");
        return redirect(controllers.routes.VolumeController.show(id));
    }

    @SecuredAction
    public Result show(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        models.Volume volume = models.Volume.find.byId(id);
        return ok(views.html.volume.show.render(currentUser, volume));
    }

    @SecuredAction
    public Result editGet(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        models.Volume volume = models.Volume.find.byId(id);
        return ok(views.html.volume.edit.render(currentUser, volume));
    }

    @SecuredAction
    public Result editPost(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        DynamicForm form = Form.form().bindFromRequest();
        String title = form.get("title");
        models.Volume volume = models.Volume.find.byId(id);
        try {
			volume.setTitle(title);
		} catch (Exception e) {
	        return ok(views.html.volume.edit.render(currentUser, volume));
		}
        return ok(views.html.volume.show.render(currentUser, volume));
    }

    @SecuredAction
    public Result delete(Long id) {
        models.Volume volume = models.Volume.find.byId(id);
        try { Ebean.delete(volume); } catch (NullPointerException ex) {}
        return redirect(controllers.routes.VolumeController.list());
    }
}