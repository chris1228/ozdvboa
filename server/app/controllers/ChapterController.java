package controllers;

import com.avaje.ebean.Ebean;
import models.*;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecureSocial;
import securesocial.core.java.SecuredAction;
import services.BookService;
import services.VolumeService;

import java.util.List;

public class ChapterController extends Controller {
	@SecuredAction
	public Result list() {
		List<models.Chapter> chapters = models.Chapter.find.all();

        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());
		return ok(views.html.Chapter.list.render(currentUser, chapters));
	}

//TODO

    @SecuredAction
    public Result newGet() {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        return ok(views.html.Chapter.new_.render(currentUser));
    }

    @SecuredAction
    public Result newPost() {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        DynamicForm form = Form.form().bindFromRequest();
        String title = form.get("chapter-title");
        String volumeId = form.get("volume-id");
        Chapter c = null;
        Volume v = null;
        Book b = null;
        try {
            c = new Chapter(title);
            v = VolumeService.findById(Long.parseLong(volumeId));
            b = BookService.getBook();
        } catch (Exception e) {
            play.Logger.error("Couldn't post new chapter " + e);
            return badRequest(views.html.book.render(currentUser, Module.BOOK, b));
        }
        c.setVolume(v);
        Ebean.save(c);
        // TODO : Return Ok
        return redirect(routes.ChapterController.show(c.getId()));
    }

    @SecuredAction
    public Result show(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());
        models.Chapter chapter = models.Chapter.find.byId(id);
        return ok(views.html.Chapter.show.render(currentUser, chapter));
    }

    @SecuredAction
    public Result editGet(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());
        models.Chapter chapter = models.Chapter.find.byId(id);
        return ok(views.html.Chapter.edit.render(currentUser, chapter));
    }

    @SecuredAction
    public Result editPost(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());
        DynamicForm form = Form.form().bindFromRequest();
        String title = form.get("chapter-title");
        models.Chapter chapter = models.Chapter.find.byId(id);
        try {
			chapter.setTitle(title);
            Ebean.update(chapter);
		} catch (Exception e) {
	        return ok(views.html.Chapter.edit.render(currentUser, chapter));
		}
        return ok(views.html.Chapter.show.render(currentUser, chapter));
    }

    @SecuredAction
    public Result delete(Long id) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());
        models.Chapter chapter = models.Chapter.find.byId(id);
        try { Ebean.delete(chapter); } catch (NullPointerException ex) {}
        return redirect(routes.ChapterController.list());
    }
}