package controllers;

import com.avaje.ebean.Ebean;
import models.Module;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Result;
import play.mvc.Controller;
import play.mvc.Results;
import securesocial.core.java.SecureSocial;
import securesocial.core.java.SecuredAction;
import services.MyUserService;

import java.util.List;

import static play.mvc.Http.Context.Implicit.ctx;

public class UserController extends Controller {

    @SecuredAction
    public Result list() {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        List<User> users = MyUserService.findAll();
        return play.mvc.Results.ok(views.html.users.list.render(currentUser, users));
    }

    @SecuredAction
    public Result show(long userId) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        User target = MyUserService.findById(userId);
        if(target == null) {
            return Results.badRequest();
        }

        return play.mvc.Results.ok(views.html.users.edit.render(currentUser, target));
    }

    @SecuredAction
    public Result showCreate() {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        return play.mvc.Results.ok(views.html.users.new_.render(currentUser, Module.USERS));
    }

    @SecuredAction
    public Result create() {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        DynamicForm form = Form.form().bindFromRequest();

        String email = form.get("email");
        String password = form.get("password");
        String firstName = form.get("firstname");
        String lastName = form.get("lastname");
        MyUserService.createNewUser(email, password, firstName, lastName);

        return redirect(controllers.routes.UserController.list());
    }

    @SecuredAction
    public Result edit(long userId) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        User target = MyUserService.findById(userId);
        if(target == null) {
            return Results.badRequest();
        }

        return play.mvc.Results.TODO;
    }

    @SecuredAction
    public Result delete(long userId) {
        User currentUser = (User) ctx().args.get(SecureSocial.USER_KEY);
        currentUser.setId(currentUser.getBasicProfile().id());

        User target = MyUserService.findById(userId);
        if(target == null) {
            return Results.badRequest();
        }

        Ebean.delete(target);

        return play.mvc.Results.TODO;
    }
}
