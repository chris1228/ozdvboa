
var gulp       = require('gulp'),
    gutil      = require('gulp-util'),
    browserify = require('browserify'),
    less       = require('gulp-less'),
    uglify     = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    buffer     = require('vinyl-buffer'),
    source     = require('vinyl-source-stream'),
    watchify   = require('watchify');

var buildDir   = '../server/public/';

var localVendorLibs = [
  'jquery-1.11.2'
].map(function(path) {
  return require.resolve('./scripts/vendor/' + path);
});

var allVendorLibs = localVendorLibs.concat(['lodash', 'q', 'spin.js']);

var devOpts = {
  // Required watchify args
  cache:{}, packageCache:{}, fullPaths: false,
  debug: true,
  insertGlobals: true,
  noParse: allVendorLibs
};

var bundler = watchify(browserify('./scripts/main.js', devOpts));

function bundle() {
  return bundler.bundle()
    .on('error', gutil.log.bind(gutil, 'Browserify error')) // Log errors
    .pipe(source('bundle.js'))
      // Sourcemaps
      .pipe(buffer())
      .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(sourcemaps.write('./')) // .map file
    .pipe(gulp.dest(buildDir));
}

function reportFinished() {
}

gulp.task('dev-scripts', function() {
  return bundle();
});

gulp.task('styles', function() {
  return gulp.src('styles/main.less')
    .pipe(less({
      filename: 'main.less',
      paths: ['styles']
    }).on('error', gutil.log))
    .pipe(gulp.dest(buildDir));
});

gulp.task('copyVendorScripts', function() {
  return gulp.src('scripts/vendor/**/*.js').pipe(gulp.dest(buildDir + '/vendor'));
});

gulp.task('copyVendorStyles', function() {
  return gulp.src('styles/vendor/**/*.*').pipe(gulp.dest(buildDir + '/vendor'));
});

gulp.task('copyAssets', function() {
  gulp.src('styles/images/**.*').pipe(gulp.dest(buildDir + '/images'));

  return gulp.src('styles/fonts/**/*.*').pipe(gulp.dest(buildDir + '/fonts'));
});


function scripts() {
  return gulp.src('scripts/main.js');
}

function watchFiles() {
  bundler.on('update', bundle);
  gulp.watch('styles/**/*.less', ['styles']);
}

var commonTasks = ['copyVendorScripts', 'copyVendorStyles', 'copyAssets', 'styles'];

gulp.task('dev', commonTasks.concat(['dev-scripts']));

gulp.task('watch', ['dev'], watchFiles);

gulp.task('default', ['watch']);
