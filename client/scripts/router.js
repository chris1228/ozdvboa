'use strict';

var Router = require('abyssa').Router;

var router = Router()
  .configure({
    interceptAnchors: !!history.pushState,
    enableLogs: false
  });

module.exports = router;
