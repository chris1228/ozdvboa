'use strict';

var newChapter = $("#new-chapter");

$("#new-chapter-btn").click(function(evt) {
    evt.preventDefault();
    newChapter.modal({
        fadeDuration: 250,
        fadeDelay: 0.10
    });
});


var newVolume = $("#new-volume");

$("#new-volume-btn").click(function(evt) {
    evt.preventDefault();
    newVolume.modal({
        fadeDuration: 250,
        fadeDelay: 0.10
    });
});
