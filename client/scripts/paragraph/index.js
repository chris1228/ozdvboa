'use strict';

var newParagraph = $("#new-paragraph");

$("#new-paragraph-btn").click(function(evt) {
    evt.preventDefault();
    newParagraph.modal({
        fadeDuration: 250,
        fadeDelay: 0.10
    });
});


$("select#paragraphWriters").select2({
    dropdownAutoWidth : true
});

$("a#paragraph-reject, a#paragraph-validate").click(function(evt){
    evt.preventDefault();
    var link = $(this).attr("href");
    $.ajax({
        url: link,
        method: "POST"
    }).done(function(resp) {
        console.log("link " + link + " okay");
        window.location.reload();
    });
});
