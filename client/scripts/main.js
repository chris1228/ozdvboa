'use strict';

require('./global');

var Q         = require('q');

Q.stopUnhandledRejectionTracking();
Q.onerror = function(error) {
  console.error(error);
};

// test/index.js
require('./test');
require('./chapter');
require('./paragraph');
