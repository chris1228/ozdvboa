# EDTC
(Instructions pour devs)  

Mettre en place l'IDE
=================================

**(Dans le répertoire `server`)**  
> 1. Lancer 'activator' (ou 'activator.bat' pour Windows) : Cela lance un nouveau terminal ou il faut taper des commandes  
> 2. Si eclipse est utilisé, taper la commande `eclipse with-source=true`  
> 3. Si IntelliJ est utilisé, taper la commande `idea with-source=true`  
> 4. Ouvrir le dossier racine du projet (celui ou se trouve le fichier ouvert actuellement `README.md`) avec l'IDE  

Développer
=================================

**(Dans le répertoire `server`)**  
> 1. Si ce n'est pas déjà fait, lancer `activator` (ou `activator.bat`)  
> 2. Lancer la commande `~run`  
> 3. Cela lance un serveur web sur le port 9000. Pour y accéder, aller à l'adresse `http://localhost:9000/` dans le navigateur de son choix (Chrome/Chromium ou Firefox svp)  
> 4. Pour arrêter, Ctrl-D, puis la touche Entrée (L'un après l'autre, pas en même temps).  

**(Dans le répertoire `client`)**  
  *Si c'est la première fois*
> 1. Installer node.js sur sa machine, puis dans un terminal lancer les commandes :  
> 2. `npm install -g gulp`  
> 3. `npm install`

  *A faire dans tous les cas*  
>`gulp` pour compiler la partie client en continu  